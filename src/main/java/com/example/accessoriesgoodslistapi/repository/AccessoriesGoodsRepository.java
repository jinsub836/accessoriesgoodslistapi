package com.example.accessoriesgoodslistapi.repository;


import com.example.accessoriesgoodslistapi.entity.AccessoriesGoods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccessoriesGoodsRepository extends JpaRepository<AccessoriesGoods,Long> {
}
