package com.example.accessoriesgoodslistapi.entity;

import com.example.accessoriesgoodslistapi.enums.GoodsCategory;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class AccessoriesGoods {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 50)
    private String thumbnailImgUrl;
    @Column(nullable = false, length = 30)
    private String goodsName;
    @Enumerated
    @Column(nullable = false)
    private GoodsCategory goodsCategory;
    @Column(nullable = false, length = 30)
    private String goodsDetail;
    @Column(nullable = false)
    private Double goodsSalePercent;
    @Column(nullable = false)
    private Double goodsPrice;
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    private String Etc;

}
