package com.example.accessoriesgoodslistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccessoriesGoodsListApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccessoriesGoodsListApiApplication.class, args);
	}

}
