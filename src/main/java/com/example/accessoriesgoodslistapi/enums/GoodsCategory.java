package com.example.accessoriesgoodslistapi.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum GoodsCategory {
    RING("반")
    ,NECKLACE("목걸이")
    ,EARRING("귀걸이");

    private final String goodsCategory;}
