package com.example.accessoriesgoodslistapi.model;

import com.example.accessoriesgoodslistapi.enums.GoodsCategory;
import jakarta.persistence.Column;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodsRequest{


    private String thumbnailImgUrl;

    private String goodsName;

    private GoodsCategory goodsCategory;

    private String goodsDetail;

    private Double goodsSalePercent;

    private Double goodsPrice;

}
