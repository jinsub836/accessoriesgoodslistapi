package com.example.accessoriesgoodslistapi.model;

import com.example.accessoriesgoodslistapi.enums.GoodsCategory;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GoodsItem {
    private Long id;

    private String thumbnailImgUrl;

    private String goodsName;

    private String goodsDetail;

    private Double goodsSalePercent;

    private Double goodsPrice;
}
