package com.example.accessoriesgoodslistapi.service;

import com.example.accessoriesgoodslistapi.entity.AccessoriesGoods;
import com.example.accessoriesgoodslistapi.model.GoodsItem;
import lombok.RequiredArgsConstructor;
import com.example.accessoriesgoodslistapi.model.GoodsRequest;
import org.springframework.stereotype.Service;
import com.example.accessoriesgoodslistapi.repository.AccessoriesGoodsRepository;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccessorisesGoodsService {
    private final AccessoriesGoodsRepository accessoriesGoodsRepository;

    public void setAccessoriesGoods (GoodsRequest request){
        AccessoriesGoods addData = new AccessoriesGoods();
        addData.setThumbnailImgUrl(request.getThumbnailImgUrl());
        addData.setGoodsName(request.getGoodsName());
        addData.setGoodsCategory(request.getGoodsCategory());
        addData.setGoodsDetail(request.getGoodsDetail());
        addData.setGoodsSalePercent(request.getGoodsSalePercent());
        addData.setGoodsPrice(request.getGoodsPrice());
        addData.setDateCreate(LocalDateTime.now());

        accessoriesGoodsRepository.save(addData);
    }

    public List<GoodsItem> getAccessoriesGoods (){
        List<AccessoriesGoods> originlist = accessoriesGoodsRepository.findAll();
        List<GoodsItem> result = new LinkedList<>();
        for (AccessoriesGoods accessoriesGoods : originlist ){
            GoodsItem addItem = new GoodsItem();
            addItem.setId(accessoriesGoods.getId());
            addItem.setThumbnailImgUrl(accessoriesGoods.getThumbnailImgUrl());
            addItem.setGoodsName(accessoriesGoods.getGoodsName());
            addItem.setGoodsDetail(accessoriesGoods.getGoodsDetail());
            addItem.setGoodsSalePercent(accessoriesGoods.getGoodsSalePercent());
            addItem.setGoodsPrice(accessoriesGoods.getGoodsPrice());

            result.add(addItem);
        }
        return result;
    }
}
