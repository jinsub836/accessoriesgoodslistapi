package com.example.accessoriesgoodslistapi.controller;

import com.example.accessoriesgoodslistapi.model.CommonResult;
import com.example.accessoriesgoodslistapi.model.GoodsItem;
import com.example.accessoriesgoodslistapi.model.GoodsRequest;
import com.example.accessoriesgoodslistapi.model.ListResult;
import com.example.accessoriesgoodslistapi.service.AccessorisesGoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/accessorises-goods")
public class AccessorisesGoodsController {
    final AccessorisesGoodsService accessorisesGoodsService;

@PostMapping("/new")
public CommonResult setAccessoriesGoods(@RequestBody GoodsRequest request) {
    accessorisesGoodsService.setAccessoriesGoods(request);

    CommonResult response = new CommonResult();
    response.setMsg("등록이 완료되었습니다.");
    response.setCode(0);

    return response;
}

@GetMapping("/all")
public ListResult<GoodsItem> getList(){
  List<GoodsItem> list = accessorisesGoodsService.getAccessoriesGoods();

    ListResult<GoodsItem> response = new ListResult<>();
    response.setList(list);
    response.setTotalCount(list.size());
    response.setCode(0);
    response.setMsg("성공하였습니다.");
    return response;
   }
}
